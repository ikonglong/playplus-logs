/*
 * Copyright (C) 2014, BeautySight Inc. All rights reserved.
 */

package com.beautysight.common.domain;

import java.io.Serializable;

/**
 * @author chenlong
 * @since 1.0
 */
public abstract class DomainEntity implements JsonAnyFieldVisible, Serializable {

}
