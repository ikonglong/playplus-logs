/*
 * Copyright (C) 2014, BeautySight Inc. All rights reserved.
 */

package com.beautysight.common.utils;

/**
 * @author chenlong
 * @since 1.0
 */
public class JsonHandlingException extends RuntimeException {

    public JsonHandlingException(String message, Throwable cause) {
        super(message, cause);
    }

}
