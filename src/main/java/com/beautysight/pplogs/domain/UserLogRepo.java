/*
 * Copyright (C) 2014, BeautySight Inc. All rights reserved.
 */

package com.beautysight.pplogs.domain;

import com.beautysight.pplogs.infrastructure.persistence.mongo.base.MongoRepository;

/**
 * @author chenlong
 * @since 1.0
 */
public interface UserLogRepo extends MongoRepository<UserLog> {

}
